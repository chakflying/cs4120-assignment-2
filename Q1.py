import glob
import spacy
from spacy import displacy
from collections import Counter

content = []
for filepath in glob.iglob('Q1/dataset/*.txt'):
    with open(filepath, encoding="utf8") as f:
        content.append(" ".join(f.readlines()))

nlp = spacy.load("en_core_web_sm")

sentences_count = 0
prepositions = Counter()
prepositions_count = 0
total_verbs_num = 0
entities_count = 0
entity_labels = {}

for doc in nlp.pipe(content, batch_size=500):
    assert doc.is_parsed
    sentences_count += len(list(doc.sents))
    for token in doc:
        if token.pos_ == "VERB":
            total_verbs_num += 1
        if token.dep_ == "prep":
            prepositions_count += 1
            prepositions[token.lemma_] += 1
    for ent in doc.ents:
        entities_count += 1
        entity_labels[ent.label_] = 1
    # displacy.serve(doc, style="dep")

with open("Q1/Q1.txt", "w") as f:
    f.write(f"Number of sentences parsed: {sentences_count}\n")
    f.write(f"Average number of verbs per sentence: {total_verbs_num / sentences_count} (POS-tag: VERB)\n")
    f.write(f"Number of prepositions: {prepositions_count}\n")
    f.write(f"Most common prepositions: {prepositions.most_common(3)}\n")
    f.write(f"Number of entities: {entities_count}\n")
    for label in entity_labels.keys():
        f.write(f"{label}\n")
