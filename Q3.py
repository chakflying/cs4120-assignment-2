from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from statistics import mean
import gensim.downloader as api
import numpy as np
import collections
import nltk
import glob
import os

content = []
for filepath in glob.iglob('Q3/training/neg/*.txt'):
    with open(filepath, encoding="utf8") as f:
        content.append({"text": " ".join(f.readlines()), "label": "neg"})

for filepath in glob.iglob('Q3/training/pos/*.txt'):
    with open(filepath, encoding="utf8") as f:
        content.append({"text": " ".join(f.readlines()), "label": "pos"})

words = []
for review in content:
    w = nltk.word_tokenize(review["text"])
    review["text_tokenized"] = w
    words.extend(w)

# %% 3.1
"""
lemmatizer = WordNetLemmatizer()
stopWords = set(stopwords.words('english'))
words = [lemmatizer.lemmatize(w).lower() for w in words if w not in stopWords and w.isalpha()]
words = collections.Counter(words).most_common(1000)
words = [word[0] for word in words]
print(f"{len(words)} words in dictionary")

X = []
Y = []
for review in content:
    bow = []
    w = review["text_tokenized"]
    w = [lemmatizer.lemmatize(word).lower() for word in w]
    for dict_w in words:
        bow.append(1) if dict_w in w else bow.append(0)
    X.append(bow)
    Y.append(review["label"])

for firstlayer in [100, 200, 500]:
    for activation in ['relu', 'logistic']:
        accuracy = []
        for i in range(10):
            x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1)
            clf = MLPClassifier(hidden_layer_sizes=(firstlayer, 10), max_iter=500, verbose=False, tol=0.000000001, activation=activation)

            clf.fit(x_train, y_train)
            y_pred = clf.predict(x_test)
            accuracy.append(accuracy_score(y_test, y_pred))
        print(f"Average accuracy for (firstlayer = {firstlayer}, activation = {activation}): {mean(accuracy)}")
"""

# %% 3.2
"""
clf = MLPClassifier(hidden_layer_sizes=(500, 10), max_iter=500, verbose=False, tol=0.000000001, activation='relu')
clf.fit(X, Y)
y_pred = clf.predict(X)
print(f"Accuracy score for the training set: {accuracy_score(Y, y_pred)}")
"""

# %% 3.3
"""
for review in content:
    w = nltk.word_tokenize(review["text"])
    review["text_tokenized"] = w
stopWords = set(stopwords.words('english'))
wv = api.load('word2vec-google-news-300')
Y = [review["label"] for review in content]
X = []
for review in content:
    wemb = np.array([0.0 for x in range(300)])
    w = review["text_tokenized"]
    w = [word.lower() for word in w if word not in stopWords and word.isalpha()]
    count = 0
    for word in w:
        try:
            wemb += wv[word]
            count += 1
        except KeyError:
            print(f"The word '{word}' does not appear in the model")
    wemb = wemb / float(count)
    X.append(list(wemb))

with open("Q3_temp.txt", "w") as f:
    for firstlayer in [300, 400, 600]:
        for activation in ['relu', 'logistic', 'tanh']:
            accuracy = []
            for i in range(10):
                x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.5)
                clf = MLPClassifier(hidden_layer_sizes=(firstlayer, 10), max_iter=800, verbose=False, tol=0.000000001,
                                    activation=activation)
                clf.fit(x_train, y_train)
                y_pred = clf.predict(x_test)
                accuracy.append(accuracy_score(y_test, y_pred))
            print(f"Average accuracy for (firstlayer = {firstlayer}, activation = {activation}): {mean(accuracy)}")
            f.write(f"Average accuracy for (firstlayer = {firstlayer}, activation = {activation}): {mean(accuracy)}")
"""

#%% 3.4
"""
X = [review["text"] for review in content]
Y = [review["label"] for review in content]
vectorizer = TfidfVectorizer(sublinear_tf=True, use_idf=True)
X = vectorizer.fit_transform(X)

for n_topics in range(12, 16):
    lsa = TruncatedSVD(n_topics)
    X_svd = lsa.fit_transform(X)
    for firstlayer in [10, 20, 30, 40]:
        for activation in ['relu', 'logistic', 'tanh']:
            accuracy = []
            for i in range(10):
                x_train, x_test, y_train, y_test = train_test_split(X_svd, Y, test_size=0.1)
                clf = MLPClassifier(hidden_layer_sizes=(firstlayer, 10), max_iter=500, verbose=False,
                                    tol=0.000000001,
                                    activation=activation)
                clf.fit(x_train, y_train)
                y_pred = clf.predict(x_test)
                accuracy.append(accuracy_score(y_test, y_pred))
            print(f'Average accuracy for (n_topics = {n_topics}, firstlayer = {firstlayer}, activation = {activation}): {mean(accuracy)}')
"""

# %% 3.5
"""
X = [review["text"] for review in content]
Y = [review["label"] for review in content]
vectorizer = TfidfVectorizer(sublinear_tf=True, use_idf=True)
X = vectorizer.fit_transform(X)
terms = vectorizer.get_feature_names()
lsa = TruncatedSVD(14)
X_svd = lsa.fit_transform(X)

for i, row in enumerate(lsa.components_[0:5]):
    row_words = []
    largest_set = sorted(range(len(row)), key=lambda i: row[i])[-20:]
    for word_i in largest_set:
        row_words.append(terms[word_i])
    print(f"most important words of row {i}: {row_words}")
"""

#%% 3.6
lemmatizer = WordNetLemmatizer()
stopWords = set(stopwords.words('english'))
words = [lemmatizer.lemmatize(w).lower() for w in words if w not in stopWords and w.isalpha()]
words = collections.Counter(words).most_common(1000)
words = [word[0] for word in words]
print(f"{len(words)} words in dictionary")

X = []
Y = []
for review in content:
    bow = []
    w = review["text_tokenized"]
    w = [lemmatizer.lemmatize(word).lower() for word in w]
    for dict_w in words:
        bow.append(1) if dict_w in w else bow.append(0)
    X.append(bow)
    Y.append(review["label"])

clf = MLPClassifier(hidden_layer_sizes=(500, 10), max_iter=800, verbose=True, tol=0.000000001, activation='relu')
clf.fit(X, Y)

test_content = []
X_test = []

for filepath in glob.iglob('Q3/test/*.txt'):
    with open(filepath, encoding="utf8") as f:
        test_content.append({"text": " ".join(f.readlines()), "filename": os.path.basename(filepath)})

for review in test_content:
    w = nltk.word_tokenize(review["text"])
    review["text_tokenized"] = w
    bow = []
    w = [lemmatizer.lemmatize(word).lower() for word in w]
    for dict_w in words:
        bow.append(1) if dict_w in w else bow.append(0)
    X_test.append(bow)

print(X_test)

y_pred = clf.predict(X_test)

with open("Q3/pos.txt", "w") as f_pos:
    with open("Q3/neg.txt", "w") as f_neg:
        for label, review in zip(y_pred, test_content):
            if label == 'pos':
                f_pos.write(review["filename"] + "\n")
            else:
                f_neg.write(review["filename"] + "\n")

