# Project environment

This project is developed in `python 3.8`.
This project uses `pipenv` for dependencies management.
`pipenv` can be installed by  `pip install --user pipenv`. Afterwards,

```bash
pipenv install
pipenv shell
```

Will launch a terminal with the correct python virtual environment. Each python file can then be run.

